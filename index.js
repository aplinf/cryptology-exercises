import data from './data.json' assert { type: 'json' };
import { inspect } from 'node:util';

function mod(n, d) {
  return ((n % d) + d) % d
}

// Ex. #1
function eea(a, b) {
  if (b === 0)
    return [a, 1, 0];

  let alpha2 = 1;
  let alpha1 = 0;
  let beta2 = 0;
  let beta1 = 1;

  while (b > 0) {
    let q = a / b;
    let r = a % b;
    let alpha1_tmp = alpha1;
    let beta1_tmp = beta1;

    alpha1 = alpha2 - q * alpha1;
    alpha2 = alpha1_tmp;
    beta1 = beta2 - q * beta1;
    beta2 = beta1_tmp;

    a = b;
    b = r;
  }

  return [a, alpha2, beta2];
}

// console.log(eea(12, 12));

// Ex. #2
const shift = {
  enc(message, key) {
    return [...message].map(char => String.fromCharCode(
      mod((char.toUpperCase().charCodeAt(0) - 65) + key, 26) + 65
    )).join('');
  },
  dec(cryptogram, key) {
    return [...cryptogram].map(char => String.fromCharCode(
      mod((char.toUpperCase().charCodeAt(0) - 65) - key, 26) + 65
    )).join('');
  }
};

console.assert(shift.dec(shift.enc('ZABC', 10), 10) === 'ZABC');


const affine = {
  enc(message, [a, b]) {
    if (eea(a, 26)[0] !== 1)
      throw new Error('not injective');

    return [...message].map(char => String.fromCharCode(
      mod((a*char.toUpperCase().charCodeAt(0) - 65) + b, 26) + 65
    )).join('');
  },
  dec(cryptogram, [a, b]) {
    if (eea(a, 26)[0] !== 1)
      throw new Error('not injective');

    return [...cryptogram].map(char => String.fromCharCode(
      mod(((char.toUpperCase().charCodeAt(0) - 65) - b) / a, 26) + 65
    )).join('');
  }
};

console.assert(affine.dec(affine.enc('ZABC', [3, 10]), [3, 10]) === 'ZABC');

// Ex. #3

function kasiskiTest(text) {
  // prepare text
  const letters = [...text]
    .filter(char => char.match(/[a-z]/i))
    .map(char => char.toUpperCase());

  // find three letter segments
  const segments = {};

  for (let i = 0; i < letters.length; i++) {
    const segment = letters.slice(i, i + 3).join('');

    if (segment.length === 3) {
      if (!segments[segment])
        segments[segment] = [];

      segments[segment].push(i);
    }
  }

  // only choose top ten three character segments
  const topSegments = Object.entries(segments)
    .sort((segmentA, segmentB) => - segmentA[1].length + segmentB[1].length)
    .slice(0,10);

  // find relative distances between segments
  for (const segment of topSegments) {
    const positions = segment[1];
    const relative = [];

    for (let i = 1; i < positions.length; i++) {
      relative.push(positions[i] - positions[i - 1]);
    }

    segment[1] = relative;
  }

  // find list of divisors for every relative distance
  const divisorsList = topSegments[0][1].map(n => {
    const divisors = [];
    
    for (let i = 3; i < n; i++) {
      if (n % i === 0) {
        divisors.push(i);
      }
    }
    
    return divisors;
  }).filter(arr => arr.length);

  // get statistics about divisor frequency from most frequent segment
  const divisorCounter = {};

  for (const divisors of divisorsList) {
    for (const divisor of divisors) {
      if (!divisorCounter[divisor])
        divisorCounter[divisor] = 0;
      
      divisorCounter[divisor]++;
    }
  }

  const topDivisors = Object.entries(divisorCounter).sort((divisorA, divisorB) => divisorB[1] - divisorA[1])
    .slice(0, 10);

  return topDivisors;
}

const segments = kasiskiTest(data.vigenerCryptogram);
// console.dir(segments);



// Ex. #4

function friedmanTest(text) {
  const counter = Object.fromEntries(data.alphabet.map(val => [val, 0]));
  
  for (const char of text)
    if (char.match(/[a-z]/i)) 
      counter[char.toUpperCase()]++;

  const freqs = Object.values(counter);
  const length = freqs.reduce((sum, n) => sum + n);

  return freqs.reduce((sum, freq) => sum + (freq * (freq - 1)), 0)
    / (length * (length - 1));
}

// console.log(friedmanTest(data.englishText));
// console.log(friedmanTest(data.englishText2));
// console.log(friedmanTest(data.englishBible));
